<?php
function upload_image(){
    $target_dir = "../images/";
    $file = $_FILES["file"]["name"];
    $file = mb_ereg_replace("([^\w\s\d\-_~,;\[\]\(\).])", '', $file);
    $file = mb_ereg_replace("([\.]{2,})", '', $file);
    $file = str_ireplace(' ', '_', $file);
    $_FILES["file"]["name"] = $file;
    $target_file = $target_dir . basename($_FILES["file"]["name"]);
    $uploadOk = 1;
    $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
    // Check if image file is a actual image or fake image
    $check = getimagesize($_FILES["file"]["tmp_name"]);
    if($check !== false) {
//        echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = 0;
    } else {
//        echo "File is not an image.";
        $uploadOk = 2;
    }
    // Allow certain file formats
    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
    && $imageFileType != "gif" ) {
//        echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
        $uploadOk = 3;
    }
    // Check if $uploadOk is greater or equal to 1 by an error
    if ($uploadOk >= 1) {
//        echo "Sorry, your file was not uploaded.";
    } else {
        if (move_uploaded_file($_FILES["file"]["tmp_name"], $target_file)) {
//            echo "The file ". basename( $_FILES["file"]["name"]). " has been uploaded.";
            list($width, $height) = getimagesize($target_file);
            if($width >= 321 && $height >= 321){
                $uploadOk = 5;
            }
        } else {
//            echo "Sorry, there was an error uploading your file.";
            $uploadOk = 4;
        }
    }

    return $uploadOk;
}