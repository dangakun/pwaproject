<div style="display: none;" id="add-form">
  <h2>Add New Item</h2>
    <sub><i class="fas fa-exclamation-triangle" style="color:orange;"></i> <i>both inputs are required</i></sub>
    <form id="addform" method="POST" enctype="multipart/form-data">
      <p>
        <label>Description: <small>(max. <em class="character-counter"></em> chars)</small></label>
        <br>
        <textarea required id="desc_text" name="desc_text" cols="50" rows="7" data-maxchar="300"></textarea>
      </p>
      <p>
        <label>Image: <small>(supported type: jpg, gif, png. max. size: 320x320 px)</small></label>
        <br>
        <input required id="image_file" name="image_file" type="file" style="width:100%;">
      </p>
      <p style="text-align: right;">
        <input id="submit_btn" type="submit">
        <input id="cancel_btn" type="button" value="Cancel" data-fancybox-close>
      </p>
    </form>
</div>