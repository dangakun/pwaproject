<?php 
include_once('../mongodb_connection.php');

$bulk = new MongoDB\Driver\BulkWrite(['ordered' => true]);

  for($i = 1; $i <= 13; $i++){
    $bulk->update(['id' => $i], ['$set' => ['order_number' => $i]]);
  }

  $result = $manager->executeBulkWrite('test.movies', $bulk);

echo json_encode(sizeof($bulk));