<?php 
include_once('../mongodb_connection.php');

//$id           = new \MongoDB\BSON\ObjectId("588c78ce02ac660426003d87");
/*
$filter      = [];
$options = [];
*/
$options = ['sort' => ['order_number' => 1]];
$query = new \MongoDB\Driver\Query($filter, $options);

$rows   = $manager->executeQuery('test.movies', $query); 
$reord = (isset($_REQUEST['reorder'])) ? boolval($_REQUEST['reorder']) : false;
$r = $rows->toArray();
if($reord == true){
  $i = 0;
  $bulk = new MongoDB\Driver\BulkWrite(['ordered' => true]);
  foreach ($r as $document) {
    $i++;
    $bulk->update(['id' => $document->id], ['$set' => ['order_number' => $i]]);
  }
  $result = $manager->executeBulkWrite('test.movies', $bulk);
}

echo json_encode($r);