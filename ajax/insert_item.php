<?php 
include_once('../mongodb_connection.php');

if( (isset($_REQUEST['desc_text'])) && (isset($_FILES['file'])) ){

  $options = array('limit' => 1,'sort' => array('id' => -1));
  $query = new \MongoDB\Driver\Query($filter, $options);
  $rows   = $manager->executeQuery('test.movies', $query);
  $r = $rows->toArray();
  $lastid = $r[0]->id;
  $lastid++;

  $options = array('limit' => 1,'sort' => array('order_number' => -1));
  $query = new \MongoDB\Driver\Query($filter, $options);
  $rows   = $manager->executeQuery('test.movies', $query);
  $r = $rows->toArray();
  $onumber = $r[0]->order_number;
  $onumber++;

  include_once('../image_upload.php');

  $ui = upload_image();

  $mname = $_REQUEST['desc_text'];

  $bulk = new MongoDB\Driver\BulkWrite(['ordered' => true]);

  if($ui == 0){
    $bulk->insert(['id' => $lastid,
      'movie_name' => $mname,
      'poster' => basename($_FILES["file"]["name"]),
      'order_number' => $onumber]);

    $result = $manager->executeBulkWrite('test.movies', $bulk);
    echo json_encode(sizeof($bulk));
  }else{
    echo $ui;
  }
}