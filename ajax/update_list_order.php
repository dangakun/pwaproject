<?php 
include_once('../mongodb_connection.php');

$bulk = new MongoDB\Driver\BulkWrite(['ordered' => true]);

if( (isset($_REQUEST['itemId'])) && (isset($_REQUEST['newPos'])) && (isset($_REQUEST['oldPos'])) ){

  $itemid = (int)$_REQUEST['itemId'];
  $npos = (int)$_REQUEST['newPos'];
  $opos = (int)$_REQUEST['oldPos'];

  if($npos != $opos){
    if($npos > $opos){
      $bulk->update(["order_number" => array('$gt' => $opos, '$lte' => $npos)], ['$inc' => ['order_number' => -1]], ['multi' => true]);
    }else{
      $bulk->update(["order_number" => array('$gte' => $npos, '$lt' => $opos)], ['$inc' => ['order_number' => 1]], ['multi' => true]);
    }

    $bulk->update(['id' => $itemid], ['$set' => ['order_number' => $npos]]);

    $result = $manager->executeBulkWrite('test.movies', $bulk);
  }
}
echo json_encode(sizeof($bulk));