<?php 
include_once('../mongodb_connection.php');

$bulk = new MongoDB\Driver\BulkWrite(['ordered' => true]);

if(isset($_REQUEST['itemId'])){

  $itemid = (int)$_REQUEST['itemId'];

  $bulk->delete(['id' => $itemid]);

  $result = $manager->executeBulkWrite('test.movies', $bulk);
}
echo json_encode(sizeof($bulk));