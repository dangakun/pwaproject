<?php 
include_once('../mongodb_connection.php');

if( (isset($_REQUEST['itemId'])) && (isset($_REQUEST['desc_text'])) ){

  $id = (int)$_REQUEST['itemId'];
  $mname = $_REQUEST['desc_text'];

  $ui = 0;

  if((isset($_FILES['file']))){
    include_once('../image_upload.php');
    $ui = upload_image();
  }

  $bulk = new MongoDB\Driver\BulkWrite(['ordered' => true]);

  if($ui == 0 && $id >= 1){

    $bulk->update(["id" => $id], ['$set' => ['movie_name' => $mname]], ['multi' => false]);
    if( (isset($_FILES['file'])) && ($ui == 0) ){
      $bulk->update(["id" => $id], ['$set' => ['poster' => basename($_FILES["file"]["name"])]], ['multi' => false]);
    }

    $result = $manager->executeBulkWrite('test.movies', $bulk);
    if(sizeof($bulk) >= 1){
      echo 0;
    }
  }else{
    echo $ui;
  }
}