<?php 
include_once('../mongodb_connection.php');

$id = (isset($_REQUEST['itemId'])) ? (int)$_REQUEST['itemId'] : 0;

$r = array();

if($id != 0){

  $filter = ['id' => $id];

  $query = new \MongoDB\Driver\Query($filter, $options);

  $rows = $manager->executeQuery('test.movies', $query); 
  $r = $rows->toArray();

}
echo json_encode($r);