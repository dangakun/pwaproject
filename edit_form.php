<div style="display: none;" id="edit-form">
  <img id="ajax-loader" src="web/css/ajax-loader.gif">
  <div id="edit_form_hidden" style="display: none;">
  <h2>Edit Item: <?=$itemid?></h2>
    <sub><i class="fas fa-exclamation-triangle" style="color:orange;"></i> <i>Description is required, leave Image empty if you don't want to change it</i></sub>
    <form id="editform" method="POST" enctype="multipart/form-data">
      <p>
        <label>Description: <small>(max. <em class="character-counter"></em> chars)</small></label>
        <br>
        <textarea required id="desc_text_edit" name="desc_text_edit" cols="50" rows="7" data-maxchar="300"></textarea>
      </p>
      <p style="text-align: center;">
        <img height="200" id="old_image_edit" src="" alt="">
      </p>
      <p>
        <label>Image: <small>(supported type: jpg, gif, png. max. size: 320x320 px)</small></label>
        <br>
        <input id="image_file_edit" name="image_file_edit" type="file" style="width:100%;">
      </p>
      <p style="text-align: right;">
        <input id="item_id_edit" name="item_id_edit" type="hidden" value="">
        <input id="submit_btn_edit" type="submit">
        <input id="cancel_btn_edit" type="button" value="Cancel" data-fancybox-close>
      </p>
    </form>
  </div>
</div>