# PWAproject

pwa project with several technologies:

Jquery
JqueryUI
MongoDB
PHP
HTML5, CSS3

Installation:

  You will need:

    -PHP 5.5.* or later
    -MongoDB (mongodb version 3.0 for php)

the mongodb database is: test

and the mongodb collection is: movies

there is a json file (movies.json) with some records for the database.


this project is made without a PHP framework due to the simplicity of the app.

	-to import the json file you will need to add the flag --jsonArray
