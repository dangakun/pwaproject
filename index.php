<?php include_once('mongodb_connection.php'); ?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>title</title>
  <link rel="stylesheet" type="text/css" href="web/libs/setCharLimit/css/main.min.css">
  <link rel="stylesheet" type="text/css" href="web/libs/jAlert/css/jquery.alerts.min.css">
  <link rel="stylesheet" type="text/css" href="web/libs/jquery/jquery-ui/jquery-ui.min.css">
  <link rel="stylesheet" type="text/css" href="web/libs/jquery/jquery-ui/jquery-ui.theme.min.css">
  <link rel="stylesheet" type="text/css" href="web/libs/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="web/libs/fancybox/css/jquery.fancybox.min.css">
  <link rel="stylesheet" type="text/css" href="web/css/Font-Awesome/web-fonts-with-css/css/fontawesome-all.min.css">
  <link rel="stylesheet" type="text/css" href="web/css/styles.css">
  </head>
  <body>
<div class="container">
	<div style="margin-top:15px;text-align:center;">
  	<i class="fas fa-list" id="countTotalItems_title" title=""></i>
  	<span class="badge" id="countTotalItems"></span>
    <input type="button" id="add_item" data-fancybox data-src="#add-form" value="Add" title="add new item">
	</div>
	<div id="item_list">
		<ul class="sortable list-group" id="itemslist"></ul>
	</div>
  <?php include_once('add_form.php') ?>
  <?php include_once('edit_form.php') ?>
</div>
	<script type="text/javascript" src='web/libs/jquery/jquery.min.js'></script>
	<script type="text/javascript" src='web/libs/jquery/jquery-ui/jquery-ui.min.js'></script>
	<script type="text/javascript" src='web/libs/bootstrap/js/bootstrap.min.js'></script>
  <script type="text/javascript" src="web/libs/fancybox/js/jquery.fancybox.min.js"></script>
  <script type="text/javascript" src="web/libs/setCharLimit/js/characterlimit.min.js"></script>
  <script type="text/javascript" src="web/libs/validation/js/jquery.validate.min.js"></script>
  <script type="text/javascript" src="web/libs/validation/js/additional-methods.min.js"></script>
  <script type="text/javascript" src="web/libs/jAlert/js/jquery.alerts.min.js"></script>
	<script type="text/javascript" src='web/js/main.js'></script>
  </body>
</html>
