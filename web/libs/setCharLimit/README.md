<h1 align="center">Character Limit</h1>

<p align="center">light weight plugin to set character limit on form elements</p>

This README would normally document whatever steps are necessary run the plugin.

Let's get started !!!

## Information ##

* Version: 1.1.0
* Developer: utsavk1993 (utsavkumar.official@gmail.com)
* Pre-requisite Library: jQuery (3.1.1)
* CSS: Bootstrap (3.3.7)

## What's included

Within the download you'll find the following files, logically grouping common assets and providing both compiled and minified variations. You'll see something like this:

```
character-limit/
├── css/
│   ├── main.css
│   ├── main.min.css
│   ├── bootstrap.min.css
└── js/
    ├── characterlimit.js
    ├── characterlimit.min.js
    └── jquery.min.js
```

## Options ##

| OPTION  | TYPE       | DEFAULT                    | OPTIONAL | DESCRIPTION                                                      |
| --------|------------|----------------------------|----------|------------------------------------------------------------------|
| counter | *string*   | character-counter          | YES      | class for the character counter element                          |

## Plugin Usage ##

**Step 1: Place the markup**
```
<input type="text" class="some-class" id="some-id" data-maxchar="10"> <!-- form element -->
<span class="some-class">
  <em>max. character allowed are</em> <!-- static message to print -->
  <em class="character-count"></em> <!-- dynamic character counter -->
</span>
```

* Add **data-maxchar** attribute to the form element/s.
* Supply a span to club two pieces of message.
* Add a tag to print dynamic character counter with class - **character-count** *(configurable using options)*.

*P.S. For point 4, ensure to pass the class name to options.*

<hr/>

**Step 2: Invoke the plugin using programmatic API**

CASE: 1 - with options

```
var options = {
  counter: 'some-class-to-add'
};
$('FORM_ELEMENT_01, FORM_ELEMENT_02').characterlimit(options);

```
CASE: 2 - without options

```
$('FORM_ELEMENT_01, FORM_ELEMENT_02').characterlimit();
```

## Change Log ##

v1.1.0

* improved coding standards using prototypes