$(document).ready(function(){

  var image_folder = 'images/';

  //with this the list can be sorted
	$('.sortable').sortable({
      placeholder: "ui-state-highlight",
      stop: function( event, ui ) { setOrderItem(event,ui); }
    });

  //to limit the characters on the textarea
  $('textarea#desc_text').characterlimit();

  //to validate the add form
  var validator = $("#addform").validate({
    submitHandler: function(form) {
      var file_data = $('#image_file').prop('files')[0];
      var form_data = new FormData();
      form_data.append('file', file_data);
      form_data.append('desc_text', $('#desc_text').val());
      submitForm(form_data,"ajax/insert_item.php");
      return false;
    }
  });

  //to validate the edit form
  var validator = $("#editform").validate({
    submitHandler: function(form) {
      var file_data = $('#image_file_edit').prop('files')[0];
      var form_data = new FormData();
      form_data.append('file', file_data);
      form_data.append('desc_text', $('#desc_text_edit').val());
      form_data.append('itemId', $('#item_id_edit').val());

      submitForm(form_data,"ajax/update_item.php");
    }
  });

  function submitForm(form_data,url_data){
    $.ajax({
      method: "POST",
      contentType: false,
      url: url_data,
      processData: false,
      data: form_data
    })
    .done(function( msg ) {
      var obj = jQuery.parseJSON( msg );
      if(msg >= 2){
        switch (msg) {
          case '2':
          case '3':
            jAlert('File is not a supported image.');
          break;
          case '4':
            jAlert('There was an erro while uploading the file.');
          break;
          case '5':
            jAlert('The size of the file is greater than 320px by 320px');
          break;
        }
      }else{
        resetAddForm();
        $.fancybox.close();
        populateItemList();
      }
      return false;
    });
  }

  $('[data-fancybox]').fancybox({
      closeExisting: true,
      smallBtn: true,
      autoFocus: true,
      backFocus: true,
      clickSlide: false,
      animationEffect: "zoom-in-out",
      beforeClose : function( instance, current, e ) {
        resetAddForm();
      }
  });

  $('#cancel_btn').on('click',function(){
    resetAddForm();
  });

  function loadEditForm(id){
    $('#ajax-loader').show();
    $('#edit_form_hidden').hide();
    //this timer is added to add some flavor to the app
    setTimeout(function(){
      $('#edit_form_hidden').show();
      var ele = $('#item_'+id);
      var itemid = ele.data('itemid');
      $.ajax({
        method: "GET",
        url: "ajax/get_item.php",
        data: { itemId: itemid }
      })
      .done(function( msg ) {
        $('#ajax-loader').hide();
        var obj = jQuery.parseJSON( msg );
        $('#item_id_edit').val(obj[0].id);
        $('#desc_text_edit').val(obj[0].movie_name);
        $('#old_image_edit').attr('src',image_folder+obj[0].poster);
      });
    }, 1100);
  }

  function resetAddForm(){
    $('#desc_text').val('');
    $('#image_file').val(null);
    validator.resetForm();
  }

  //functions executed at start
  populateItemList();

  //update list with all the items
  function populateItemList(reord=0){
    //call ajax with items list json
    $.ajax({
      method: "GET",
      url: "ajax/get_item_list.php",
      data: { reorder: reord }
    })
    .done(function( msg ) {
      var obj = jQuery.parseJSON( msg );
      $("#itemslist").empty();
      for(item in obj){
        var itemid = obj[item].id;
        var itemdesc = obj[item].movie_name;
        var itemimage = obj[item].poster;
        var itemordernumber = obj[item].order_number;
        var li = '<li id="item_'+itemid+'" data-itemname="'+itemdesc+'" data-itemid="'+itemid+'" data-pos="'+itemordernumber+'" class="itemli list-group-item ui-sortable-handle"><div class="itemdiv">'+
        '<span class="item_id">'+itemid+'</span>&nbsp;'+
        '<div><img class="item_image" height="150" src="images/'+itemimage+'"></div>&nbsp;'+
        '<span class="list_buttons">'+
        '<i class="edit_item far fa-edit" data-fancybox data-src="#edit-form" data-itemid="'+itemid+'"></i>&nbsp;'+
        '<i class="delete_item fas fa-trash-alt" data-itemid="'+itemid+'"></i>'+
        '</span>'+
        '<div class="itemname">'+itemdesc+'</div>&nbsp;'+
        '</div></li>';
        $("#itemslist").append(li);
      }
      reloadItemListCounter();
    });
  }

  //update item order posotion on the list, conect to mongodb throught php with ajax.
  function setOrderItem(ev,ui){
    var id = $('#'+ui.item[0].id).data('itemid');
    var p = $('#'+ui.item[0].id).data('pos');
    var opos = $('#'+ui.item[0].id).data('pos');
    var npos = 0;
    var nextsib = ui.item[0].nextSibling;
    if(getType(nextsib) != 'htmllielement'){
      prevsib = ui.item[0].previousSibling.id;
      prevsib = $('#'+prevsib).data('pos');
      npos = prevsib;
    }else{
      nextsib = ui.item[0].nextSibling.id;
      nextsib = $('#'+nextsib).data('pos');
      npos = nextsib;
      if(npos > opos){
        npos--;
      }
    }
    $.ajax({
      method: "GET",
      url: "ajax/update_list_order.php",
      data: { itemId: id, newPos: npos, oldPos: opos}
    })
    .done(function( msg ) {
      var obj = jQuery.parseJSON( msg );
      populateItemList();
    });
  }

  //update item order posotion on the list, conect to mongodb throught php with ajax.
  function reloadItemListCounter(){
    $.ajax({
      method: "GET",
      url: "ajax/get_total_items.php"
    })
    .done(function( msg ) {
      var obj = jQuery.parseJSON( msg );
      $('#countTotalItems').html(obj);
      $('#countTotalItems_title').attr('title','total: '+obj);

    });
  }

  //this is needed for the added buttons on the list
  $(document).on('click','.delete_item',function(){
    deleteItem($(this).data('itemid'));
  });

  //this is needed for the added buttons on the list
  $(document).on('click','.edit_item',function(){
    loadEditForm($(this).data('itemid'));
  });


  function deleteItem(id){
    var itext = $('#item_'+id).data('itemname');
    var iid = $('#item_'+id).data('itemid');
    if(itext.length >= 20){
      itext = itext.substr(0,20).trim();
    }
    jConfirm('Do you want to delete this? <br>'+itext, 'Confirmation Dialog', function(r) {
      //if click on Confirm then delete the item on the list and trigger the reload item list counter function
      if(r == true){
        $.ajax({
          method: "GET",
          url: "ajax/delete_item.php",
          data: { itemId: iid }
        })
        .done(function( msg ) {
          if(msg == 1){
            populateItemList(true);
          }
        });
      }
    });
  }

  function getType (val) {
      if (typeof val === 'undefined') return 'undefined';
      if (typeof val === 'object' && !val) return 'null';
      return ({}).toString.call(val).match(/\s([a-zA-Z]+)/)[1].toLowerCase();
  }

});